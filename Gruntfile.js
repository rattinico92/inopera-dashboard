module.exports = function (grunt) {

    // laod list of javascript files to be used
    // let javascripts = grunt.file.readJSON("grunt-list-javascripts.json");

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // compile sass files
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'assets/src/css',
                    src: [
                        'app.scss',
                        'dashboard.scss',
                        'login.scss',
                        'table.scss',
                        'scheda.scss'
                    ],
                    dest: 'assets/dist/css',
                    ext: '.css'
                }]
            }
        },

        // cssmin: {
        //     options: {
        //         sourceMap: true,
        //         rebase: true,
        //         rebaseTo: '<%= paths.css.dist %>'
        //     },
        //     all: {
        //         files: [{
        //             src: ['<%= paths.css.src %>'],
        //             dest: '<%= paths.css.dist %>/styles.min.css',
        //         }]
        //     }
        // },

        // jshint: {
        //     all: ['<%= paths.js.validate %>'],
        // },

        // uglify: {
        //     options: {
        //         banner: '/*! \n\t<%= pkg.name %> \n\tPack version: <%= pkg.version %>\n\tBuild time: <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
        //     },
        //     minjs: {
        //         options: {
        //             mangle: true,
        //             compress: true,
        //             sourceMap: true,
        //         },
        //         src: '<%= paths.js.src %>',
        //         dest: '<%= paths.js.dist %>/scripts.min.js'
        //     }
        // },

        watch: {
            // js: {
            //     files: '<%= paths.js.src %>',
            //     tasks: ['js'],
            //     options: {
            //         spawn: false,
            //     },
            // },
            sass: {
                files: ['assets/src/css/**/*.sass', 'assets/src/css/**/*.scss', 'assets/src/css/**/*.css'],
                tasks: ['css'],
                options: {
                    spawn: false,
                },
            },
        }

    });


    // Load the plugins that provides the defined tasks.
    grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.loadNpmTasks('grunt-contrib-jshint');
    // grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    // grunt.loadNpmTasks('grunt-contrib-cssmin');


    // Default task(s).
    grunt.registerTask('default', ['all']);

    // grunt.registerTask('css', ['sass', 'cssmin']);
    grunt.registerTask('css', ['sass']);
    // grunt.registerTask('js', ['jshint', 'uglify']);
    // grunt.registerTask('all', ['js', 'css']);
    grunt.registerTask('all', ['css']);


};